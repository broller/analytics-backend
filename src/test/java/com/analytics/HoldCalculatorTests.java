package com.analytics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Test;

import com.analytics.holds.HoldsCalculationService;

public class HoldCalculatorTests {
	@Test
	void holdTests() {
		assertEquals(round(HoldsCalculationService.calculateHold(-150, 130)), 3.36);
		assertEquals(round(HoldsCalculationService.calculateHold(-109, -112)), 4.75);
		assertEquals(round(HoldsCalculationService.calculateHold(170, -215)), 5.03);
	}
	
	double round(double value) {
	    BigDecimal bd = BigDecimal.valueOf(value * 100);
	    bd = bd.setScale(2, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
