package com.analytics.python;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

@Service
public class PythonService {
	public String runPythonScript(String scriptPath, String [] cmdArgs) throws IOException {
		String pythonScriptPath = scriptPath;
		String[] cmd = new String[2];
		cmd[0] = "/usr/local/bin/python3";
		cmd[1] = pythonScriptPath;
		String message = "";
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr;
		String line = "";
		String err = null;
		
		try {
			pr = rt.exec(ArrayUtils.addAll(cmd, cmdArgs));
			
			// retrieve output from python script
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while((line = bfr.readLine()) != null) {
				// display each output line form python script
				message += line;
			}
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			while((line = errReader.readLine()) != null) {
				err += line;
			}
			
			if(err != null) {
				System.out.println(err);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return message;
	}
}
