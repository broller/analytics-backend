package com.analytics.stats;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analytics.holds.HoldsService;
import com.analytics.models.Affiliates;
import com.analytics.models.Event;
import com.analytics.models.Market;
import com.analytics.python.PythonService;
import com.analytics.sports.Leagues;
import com.analytics.sports.TeamService;
import com.analytics.stats.models.SimulationResults;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PaceModelService {
	@Autowired
	private HoldsService holdsService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private PythonService pythonService;
	
	private final String paceModelScript = "/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/basketball/models/pace-model.py";
	
	public HashMap<Affiliates, ArrayList<Event>> getGameResults() {
		HashMap<Affiliates, ArrayList<Event>> allAffiliatesList = new HashMap<Affiliates, ArrayList<Event>>();
		for (Affiliates aff : Affiliates.values()) { 
			ArrayList<Event> list = null;
			switch(aff) {
			    case DRAFTKINGS:
			    	list = holdsService.getHoldsByAffiliate("dk_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.DRAFTKINGS, list);
			    	break;
			    case FANDUEL:
			    	list = holdsService.getHoldsByAffiliate("fd_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.FANDUEL, list);
			    	break;
			    case POINTSBET:
			    	list = holdsService.getHoldsByAffiliate("pb_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.POINTSBET, list);
			    	break;
		    	default:
		    		break;
		    }
		}
		return getModelResults(allAffiliatesList);
	}
	
	private HashMap<Affiliates, ArrayList<Event>> getModelResults(HashMap<Affiliates, ArrayList<Event>> events) {
		ExecutorService executor = Executors.newFixedThreadPool(events.keySet().size());
		
		events.forEach((key,eventList) -> {
			executor.submit(() -> {
				eventList.forEach(ev -> {
					try {
						if(ev.getMarket("spread") != null && ev.getMarket("moneyline") != null && ev.getMarket("total") != null) {
							String result = pythonService.runPythonScript(paceModelScript, eventToPaceModelArgs(ev));
							ev.setSimulationResults(toPaceResultObject(result));	
						}
					} catch (IOException e) {
						// TODO log which events failed
						e.printStackTrace();
					}
				});
				return;
			});
		});
		executor.shutdown();
		try {
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		 e.printStackTrace();
		}
		return events;
	}
	
	private String [] eventToPaceModelArgs(Event event) {
		Market spread = event.getMarket("spread");
		Market ml  = event.getMarket("moneyline");
		Market total = event.getMarket("total");
		
		String teams = teamService.getTeamID(Leagues.NBA, spread.getSides().get(0).getTeamName(), "NBATeamKeys.json", "basketballReference") + " " + teamService.getTeamID(Leagues.NBA, spread.getSides().get(1).getTeamName(), "NBATeamKeys.json", "basketballReference");
		String overUnder = String.valueOf(total.getSides().get(0).getPoints());
		String homeSpread = String.valueOf(spread.getSides().get(1).getPoints());
		String mlOdds = String.valueOf(ml.getSides().get(0).getOdds()) + " " + String.valueOf(ml.getSides().get(1).getOdds());
		String totalOdds = String.valueOf(total.getSides().get(0).getOdds()) + " " + String.valueOf(total.getSides().get(1).getOdds());
		String spreadOdds = String.valueOf(spread.getSides().get(0).getOdds()) + " " + String.valueOf(spread.getSides().get(1).getOdds());
		
		String[] cmdArgs = (teams + " " + overUnder + " " + homeSpread + " " + mlOdds + " " + totalOdds + " " + spreadOdds).split(" ");
		return cmdArgs;
	}
	
	private SimulationResults toPaceResultObject(String obj) {
		ObjectMapper mapper = new ObjectMapper();
		SimulationResults readValue = null;
		try {
			readValue = mapper.readValue(obj, SimulationResults.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return readValue;
	}
}
