package com.analytics.stats.models;

public class SimulationResults {
	private double homeMLEdge;
	private double homeMLPct;
	private double homeSpreadEdge;
	private double homeSpreadPct;
	private double overEdge;
	private double overPct;
	private double underEdge;
	private double underPct;
	private double awayMLEdge;
	private double awayMLPct;
	private double awaySpreadEdge;
	private double awaySpreadPct;
	public double getHomeMLEdge() {
		return homeMLEdge;
	}
	public void setHomeMLEdge(double homeMLEdge) {
		this.homeMLEdge = homeMLEdge;
	}
	public double getHomeMLPct() {
		return homeMLPct;
	}
	public void setHomeMLPct(double homeMLPct) {
		this.homeMLPct = homeMLPct;
	}
	public double getHomeSpreadEdge() {
		return homeSpreadEdge;
	}
	public void setHomeSpreadEdge(double homeSpreadEdge) {
		this.homeSpreadEdge = homeSpreadEdge;
	}
	public double getHomeSpreadPct() {
		return homeSpreadPct;
	}
	public void setHomeSpreadPct(double homeSpreadPct) {
		this.homeSpreadPct = homeSpreadPct;
	}
	public double getOverEdge() {
		return overEdge;
	}
	public void setOverEdge(double overEdge) {
		this.overEdge = overEdge;
	}
	public double getOverPct() {
		return overPct;
	}
	public void setOverPct(double overPct) {
		this.overPct = overPct;
	}
	public double getUnderEdge() {
		return underEdge;
	}
	public void setUnderEdge(double underEdge) {
		this.underEdge = underEdge;
	}
	public double getUnderPct() {
		return underPct;
	}
	public void setUnderPct(double underPct) {
		this.underPct = underPct;
	}
	public double getAwayMLEdge() {
		return awayMLEdge;
	}
	public void setAwayMLEdge(double awayMLEdge) {
		this.awayMLEdge = awayMLEdge;
	}
	public double getAwayMLPct() {
		return awayMLPct;
	}
	public void setAwayMLPct(double awayMLPct) {
		this.awayMLPct = awayMLPct;
	}
	public double getAwaySpreadEdge() {
		return awaySpreadEdge;
	}
	public void setAwaySpreadEdge(double awaySpreadEdge) {
		this.awaySpreadEdge = awaySpreadEdge;
	}
	public double getAwaySpreadPct() {
		return awaySpreadPct;
	}
	public void setAwaySpreadPct(double awaySpreadPct) {
		this.awaySpreadPct = awaySpreadPct;
	}
}
