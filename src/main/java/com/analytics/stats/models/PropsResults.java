package com.analytics.stats.models;

public class PropsResults {
	private String playerName;
	private double overOdds;
	private double underOdds;
	private double value;
	private double overEdge;
	private double underEdge;
	
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public double getOverOdds() {
		return overOdds;
	}
	public void setOverOdds(double overOdds) {
		this.overOdds = overOdds;
	}
	public double getUnderOdds() {
		return underOdds;
	}
	public void setUnderOdds(double underOdds) {
		this.underOdds = underOdds;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public double getOverEdge() {
		return overEdge;
	}
	public void setOverEdge(double overEdge) {
		this.overEdge = overEdge;
	}
	public double getUnderEdge() {
		return underEdge;
	}
	public void setUnderEdge(double underEdge) {
		this.underEdge = underEdge;
	}
}
