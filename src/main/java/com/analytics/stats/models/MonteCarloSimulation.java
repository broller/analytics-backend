package com.analytics.stats.models;

public class MonteCarloSimulation {
	private String favorite;
    private String underdog;
    private double total;
    private int overOdds;
    private int underOdds;
    private double spread;
    private int favoriteSpreadOdds;
    private int underdogSpreadOdds;
    private int favoriteMLOdds;
    private int underdogMLOdds;
    
    public void setFavorite(String favorite) {
		this.favorite = favorite;
	}

	public void setUnderdog(String underdog) {
		this.underdog = underdog;
	}
	
	public String getFavorite() {
		return favorite;
	}

	public String getUnderdog() {
		return underdog;
	}

	public double getTotal() {
		return total;
	}

	public int getUnderOdds() {
		return underOdds;
	}

	public int getOverOdds() {
		return overOdds;
	}

	public int getFavoriteSpreadOdds() {
		return favoriteSpreadOdds;
	}

	public double getSpread() {
		return spread;
	}

	public int getUnderdogSpreadOdds() {
		return underdogSpreadOdds;
	}

	public int getFavoriteMLOdds() {
		return favoriteMLOdds;
	}

	public int getUnderdogMLOdds() {
		return underdogMLOdds;
	}
	
	public String getMonteCarloArgs() {
		return favorite + " " +  underdog + " PTS " + total + " " + spread + " " + favoriteMLOdds + " " + underdogMLOdds + " "
				+ overOdds + " " + underOdds + " " + favoriteSpreadOdds + " " + underdogSpreadOdds;
	}
}
