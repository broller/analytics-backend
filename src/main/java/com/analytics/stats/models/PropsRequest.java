package com.analytics.stats.models;

import org.apache.commons.lang3.ArrayUtils;
import com.analytics.models.Affiliates;

public class PropsRequest {
	private Affiliates affiliate;
	private  String eventId;
	private String [] teams;
	public Affiliates getAffiliate() {
		return affiliate;
	}
	public void setAffiliate(Affiliates affiliate) {
		this.affiliate = affiliate;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String [] getTeams() {
		return teams;
	}
	public void setTeams(String [] teams) {
		this.teams = teams;
	}
	public String [] getArgs() {
		return ArrayUtils.addAll(new String []{ this.getAffiliate().toString() , this.getEventId()  }, this.teams);
	}
}
