package com.analytics.stats.models;

public class MonteCarloResults {
	private double favoriteMLPct;
	private double favoriteMLEdge;
	private double underdogMLPct;
	private double underdogMLEdge; 
	private double overPct;
	private double overEdge;
	private double underPct;
	private double underEdge;
	private double favoriteSpreadPct;
	private double favoriteSpreadEdge;
	private double underdogSpreadPct;
	private double underdogSpreadEdge;
	
	public double getFavoriteMLPct() {
		return favoriteMLPct;
	}

	public double getFavoriteMLEdge() {
		return favoriteMLEdge;
	}

	public double getUnderdogMLPct() {
		return underdogMLPct;
	}

	public double getUnderdogMLEdge() {
		return underdogMLEdge;
	}

	public double getOverPct() {
		return overPct;
	}

	public double getUnderPct() {
		return underPct;
	}

	public double getUnderEdge() {
		return underEdge;
	}

	public double getFavoriteSpreadPct() {
		return favoriteSpreadPct;
	}

	public double getFavoriteSpreadEdge() {
		return favoriteSpreadEdge;
	}

	public double getUnderdogSpreadPct() {
		return underdogSpreadPct;
	}

	public double getUnderdogSpreadEdge() {
		return underdogSpreadEdge;
	}

	public double getOverEdge() {
		return overEdge;
	}
}
