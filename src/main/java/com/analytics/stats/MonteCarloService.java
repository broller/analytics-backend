package com.analytics.stats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

import com.analytics.stats.models.MonteCarloResults;
import com.analytics.stats.models.MonteCarloSimulation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MonteCarloService {
	public MonteCarloResults runNBAGameSim(MonteCarloSimulation simInfo) {
		String pythonScriptPath = "/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/basketball/normalized-monte-carlo.py";
		String[] cmd = new String[2];
		cmd[0] = "/usr/local/bin/python3";
		cmd[1] = pythonScriptPath;
		String [] args = simInfo.getMonteCarloArgs().split(" ");
		String message = "";
		String err = null;
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr;
		String line = "";
		try {
			pr = rt.exec(ArrayUtils.addAll(cmd, args));
			pr.waitFor();
			
			// retrieve output from python script
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while((line = bfr.readLine()) != null) {
				message += line;
			}
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			while((line = errReader.readLine()) != null) {
				err += line;
			}
			
			if(err != null) {
				System.out.println(err);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return toMCResultObject(message);
	}

	private MonteCarloResults toMCResultObject(String obj) {
		ObjectMapper mapper = new ObjectMapper();
		MonteCarloResults readValue = null;
		try {
			readValue = mapper.readValue(obj, MonteCarloResults.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return readValue;
	}
}
