package com.analytics.stats;


import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analytics.models.Affiliates;
import com.analytics.models.Event;
import com.analytics.sports.Leagues;
import com.analytics.sports.TeamService;
import com.analytics.stats.models.MonteCarloResults;
import com.analytics.stats.models.MonteCarloSimulation;
import com.analytics.stats.models.PropsRequest;
import com.analytics.stats.models.PropsResults;

@RestController
@RequestMapping("/stats")
public class StatsController {
	
	@Autowired
	private PropsService propsService;
	
	@Autowired
	private MonteCarloService monteCarloService;
	
	@Autowired
	private PaceModelService paceModelService;
	
	@Autowired
	private TeamService teamService;
	
	@PostMapping("/{league}/game")
	@CrossOrigin(origins = "http://localhost:4200")
	public MonteCarloResults getSimulationResults(@PathVariable("league") Leagues league, @RequestBody MonteCarloSimulation simInfo) {
		if(league == Leagues.NBA) {
			return monteCarloService.runNBAGameSim(simInfo);
		}
		return null;
	}
	
	@PostMapping("/{league}/affiliate/game")
	@CrossOrigin(origins = "http://localhost:4200")
	public MonteCarloResults getSimResultsByAffiliate(@PathVariable("league") Leagues league, @RequestBody MonteCarloSimulation simInfo) {
		if(league == Leagues.NBA) {
			simInfo.setFavorite(teamService.getTeamID(Leagues.NBA, simInfo.getFavorite(), "NBATeamKeys.json", "basketballReference"));
			simInfo.setUnderdog(teamService.getTeamID(Leagues.NBA, simInfo.getUnderdog(), "NBATeamKeys.json", "basketballReference"));
			return monteCarloService.runNBAGameSim(simInfo);
		}
		return null;
	}
	
	@GetMapping("/{league}/affiliate/model/pace")
	@CrossOrigin(origins = "http://localhost:4200")
	public HashMap<Affiliates, ArrayList<Event>> getPaceModelResults(@PathVariable("league") Leagues league) {
		return paceModelService.getGameResults();
	}
	
	
	@PostMapping("/{league}/game/props/")
	@CrossOrigin(origins = "http://localhost:4200")
	public PropsResults[] getPropsByGame(@PathVariable("league") Leagues league, @RequestBody PropsRequest request) {
		return propsService.getPropsResults(request);
	}
}
