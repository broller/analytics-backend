package com.analytics.stats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analytics.sports.Leagues;
import com.analytics.sports.TeamService;
import com.analytics.stats.models.PropsRequest;
import com.analytics.stats.models.PropsResults;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PropsService {
	@Autowired
	private TeamService teamService;
	
	public PropsResults[] getPropsResults(PropsRequest request) {
		String pythonScriptPath = "/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/basketball/props/props_model.py";
		String[] cmd = new String[2];
		cmd[0] = "/usr/local/bin/python3";
		cmd[1] = pythonScriptPath;
		
		ArrayList<String> formattedTeams = new ArrayList<String>();
		for(String team : request.getTeams()) {
			formattedTeams.add(teamService.getTeamID(Leagues.NBA, team, "NBATeamKeys.json", "basketballReference"));
		}
		request.setTeams((String[])formattedTeams.toArray(new String[0]));
		
		String[] args = request.getArgs();
		String err = null;
		String message = "";
		
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr;
		String line = "";
		try {
			pr = rt.exec(ArrayUtils.addAll(cmd, args));
			pr.waitFor();
			
			// retrieve output from python script
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while((line = bfr.readLine()) != null) {
				message += line;
			}
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			while((line = errReader.readLine()) != null) {
				err += line;
			}
			
			if(err != null) {
				System.out.println("Printing error");
				System.out.println(err);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return parsePropResults(message);
	}
	
	private PropsResults[] parsePropResults(String results) {
		ObjectMapper mapper = new ObjectMapper();
		PropsResults[] readValue = null;
		try {
			readValue = mapper.readValue(results, PropsResults[].class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return readValue;
	}
}
