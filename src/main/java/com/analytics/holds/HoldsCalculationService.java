package com.analytics.holds;

public class HoldsCalculationService {
	public static double calculateBreakEvenPercentage(double odds) {
		Double absValOdds = Math.abs(odds);
		if(odds > 0) {
			return 100 / ( (absValOdds / 100) + 1);
		} else {
			return 100 / ( (100 / absValOdds) + 1);
		}
	}
	
	public static double calculateHold(double team1Odds, double team2Odds) {
		double team1PTWRatio = team1Odds > 0 ? Math.abs(team1Odds) / 100 : 100 / Math.abs(team1Odds); // .667
		double team2PTWRatio = team2Odds > 0 ? Math.abs(team2Odds) / 100 : 100 / Math.abs(team2Odds); // 1.3
		
		double team1WagerPlaced, team2WagerPlaced, team1PotentialPayout,
			team2PotentialPayout, team1PotentialProfit, team2PotentialProfit, hold; //, team2Hold;
		
		team1WagerPlaced = team1Odds < 0 ? Math.abs(team1Odds) : 100;
		team1PotentialPayout = team1WagerPlaced * team1PTWRatio;
		team2WagerPlaced = (team1WagerPlaced + team1PotentialPayout) / (1 + team2PTWRatio);
		team2PotentialPayout = team2WagerPlaced * team2PTWRatio;
		team1PotentialProfit = team2WagerPlaced - team1PotentialPayout;
		team2PotentialProfit = team1WagerPlaced - team2PotentialPayout;
		hold = team1PotentialProfit / (team1WagerPlaced + team2WagerPlaced);
//		team2Hold = team2PotentialProfit / (team1WagerPlaced + team2WagerPlaced);
		
//		System.out.println("team1WagerPlaced: " + team1WagerPlaced);
//		System.out.println("team1PotentialPayout: " + team1PotentialPayout);
//		System.out.println("team2WagerPlaced: " + team2WagerPlaced);
//		System.out.println("team2PotentialPayout: " + team2PotentialPayout);
//		System.out.println("team1PotentialProfit: " + team1PotentialProfit);
//		System.out.println("team2PotentialProfit: " + team2PotentialProfit);
//		System.out.println("team1Hold " + team1Hold);
//		System.out.println("team2Hold " + team2Hold);
		
		return hold;
	}
}