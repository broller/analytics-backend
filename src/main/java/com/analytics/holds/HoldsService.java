package com.analytics.holds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.analytics.models.Event;
import com.analytics.models.Hold;
import com.analytics.models.Market;
import com.analytics.models.Side;
import com.analytics.sports.NBATeams;


@Service
public class HoldsService {
	public ArrayList<Event> getHoldsByAffiliate(String fileName) {
		String pythonScriptPath = "/Users/brandonr/Software_Projects/wagering_tools/scrapers/" + fileName;
		String[] cmd = new String[2];
		cmd[0] = "/usr/local/bin/python3";
		cmd[1] = pythonScriptPath;
		String message = "";
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr;
		String line = "";
		String err = null;
		try {
			pr = rt.exec(cmd);
			
			// retrieve output from python script
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			while((line = bfr.readLine()) != null) {
				// display each output line form python script
				message += line;
			}
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			while((line = errReader.readLine()) != null) {
				err += line;
			}
			
			if(err != null) {
				System.out.println(err);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return parseJSON(message);
	}
	
	private ArrayList<Event> parseJSON(String s) {
		JSONObject obj = new JSONObject(s);
		Iterator<String> keys = obj.keys();
		ArrayList<Event>list = new ArrayList<Event>();

		while(keys.hasNext()) {
		    String key = keys.next();
		    if (obj.get(key) instanceof JSONArray) {
		    	JSONArray arr = obj.getJSONArray(key); 
		    	JSONObject side1 = arr.getJSONObject(0);
		    	JSONObject side2 = arr.getJSONObject(1);
		    	
		    	Event e = new Event();
		    	
		    	e.setEventId(getEventId(side1));
		    	
		    	Market spreadMarket = getSpreadHold(side1, side2);
		    	if(spreadMarket != null) {
			    	e.addMarket("spread", spreadMarket);	
		    	}
		    	
		    	Market totalMarket = getTotalHold(side1, side2);
		    	if(totalMarket != null) {
		    		e.addMarket("total", totalMarket);   		
		    	}
		 
		    	Market moneylineMarket = getMoneylineHold(side1, side2);
		    	if(moneylineMarket != null) {
			    	e.addMarket("moneyline", moneylineMarket);	
		    	}
		    	list.add(e);
		    }
		}
		return list;
	}
	
	private String getEventId(JSONObject side) {
		return side.getString("eventId");
	}
	
	private Market getSpreadHold(JSONObject side1, JSONObject side2) {
		Side s1 = getSpread(side1);
		Side s2 = getSpread(side2);
		
		if(s1 != null && s2 != null) {
			return new Hold(s1, s2);
		}
		return null;
	}
	
	private Side getSpread(JSONObject side) {
		JSONObject spread;
		String name = normalizeTeamName(side.getString("team")); // side.getString("team");
		try {
			spread = side.getJSONObject("spread");	
		} catch(JSONException e) {
			return null;
		}
		return new Side(name, spread.getDouble("points"), spread.getDouble("odds"));
	}
	
	private Market getTotalHold(JSONObject side1, JSONObject side2) {
		Side s1 = getTotal(side1);
		Side s2 = getTotal(side2);
		if(s1 != null && s2 != null) {
			return new Hold(s1, s2);
		}
		
		return null;
	}
	
	private Side getTotal(JSONObject side) {
		JSONObject total;
		String name = normalizeTeamName(side.getString("team")); // side.getString("team");
		try {
			total = side.getJSONObject("total");	
		} catch(JSONException e) {
			return null;
		}
		return new Side(name, total.getDouble("total"), total.getDouble("odds"), total.getString("overUnder"));
	}
	
	private Market getMoneylineHold(JSONObject side1, JSONObject side2) {
		Side s1 = getMoneylineSide(side1);
		Side s2 = getMoneylineSide(side2);
		if(s1 != null && s2 != null) {
			return new Hold(s1, s2);
		}
		
		return null;
	}
	
	private Side getMoneylineSide(JSONObject side) {
		double odds;
		String name = normalizeTeamName(side.getString("team")); // side.getString("team");
		try {
			odds = side.getDouble("moneyline");
		} catch(JSONException e) {
			return null;
		}
		return new Side(name, 0, odds);
	}
	
	private String normalizeTeamName(String name) {
		String [] parts = name.trim().split(" ");
		NBATeams team = NBATeams.stream().filter(t -> t.toString().contains(parts[parts.length - 1]))
		.findAny().orElse(null);
		
		return team != null ? team.toString().replaceAll("_", " ") : name;
	}
}
