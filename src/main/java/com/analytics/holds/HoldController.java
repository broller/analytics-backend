package com.analytics.holds;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analytics.models.Affiliates;
import com.analytics.models.Event;

@RestController
public class HoldController {
	
	@Autowired
	private HoldsService holdsService;
	
	@GetMapping("/holds")
	@CrossOrigin(origins = "http://localhost:4200")
	public HashMap<Affiliates, ArrayList<Event>> getHolds() {
		HashMap<Affiliates, ArrayList<Event>> allAffiliatesList = new HashMap<Affiliates, ArrayList<Event>>();
		for (Affiliates aff : Affiliates.values()) { 
			ArrayList<Event> list = null;
			switch(aff) {
			    case DRAFTKINGS:
			    	list = holdsService.getHoldsByAffiliate("dk_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.DRAFTKINGS, list);
			    	break;
			    case FANDUEL:
			    	list = holdsService.getHoldsByAffiliate("fd_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.FANDUEL, list);
			    	break;
			    case POINTSBET:
			    	list = holdsService.getHoldsByAffiliate("pb_main_line_reader.py");
			    	allAffiliatesList.put(Affiliates.POINTSBET, list);
			    	break;
		    	default:
		    		break;
		    }
		}
		return allAffiliatesList;
	}
}