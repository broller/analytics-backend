package com.analytics.records;

import java.util.stream.Stream;


public enum WagerTypes {
	SPREAD("spread"),
	TOTAL("total"),
	MONEYLINE("moneyline");
	
	private final String wagerType;

    /**
     * @param name
     */
    private WagerTypes(final String wagerType) {
        this.wagerType = wagerType;
    }

    public String getWagerType() {
        return wagerType;
    }

	public static Stream<WagerTypes> stream() {
        return Stream.of(WagerTypes.values()); 
    }
}
