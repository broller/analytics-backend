package com.analytics.records;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analytics.records.models.EventRecordRequest;
import com.analytics.records.models.PropRecordUpdateRequest;
import com.analytics.records.models.RecordUpdateRequest;
import com.analytics.records.models.WagerRecord;
import com.analytics.records.templates.WagerRecordTemplate;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesResponse;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RestController
@RequestMapping("/records")
public class RecordsController {
	private static final String APPLICATION_NAME = "Wager Record Writer";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens"; 
    private static final String RECORD_ID = "1A9g_IXPg3zIgjgOTz0-0lD3DoYZv1ZpJIFqNXKPEcXo";
    private static final String PACE_MODEL_RECORDS_ID = "1k2YckYi8izLknScEgV5h7ZHSRnqUaYA0kYh1H7yRxNs";
    private static final String PROPS_RECORD_ID = "1rfKe0ga6tT6zClcbmW3j9hpHZP-Gjuf4LDlZgUNv4MQ";
   
    // Value of this SCOPES variable will decide the reading and writing rights
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH = "/google-sheets-client-secret.json";
    
    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = RecordsController.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        
        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType(WagerRecordTemplate.ACCESS_TYPE_OFFLINE)
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
    
	@PostMapping("/create")
	public boolean createRecord(@RequestBody String fileName) {
		try {
			// Build a new authorized API client service.
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
			        .setApplicationName(APPLICATION_NAME)
			        .build();
        
			Spreadsheet spreadsheet = new Spreadsheet()
                .setProperties(new SpreadsheetProperties().setTitle(fileName))
                .setSheets(
            		List.of(
            				new Sheet().setProperties(new SheetProperties().setTitle(WagerTypes.SPREAD.getWagerType())),
            				new Sheet().setProperties(new SheetProperties().setTitle(WagerTypes.TOTAL.getWagerType())),
            				new Sheet().setProperties(new SheetProperties().setTitle(WagerTypes.MONEYLINE.getWagerType()))
            		)
                );
        
			spreadsheet = service.spreadsheets().create(spreadsheet)
			        .setFields(WagerRecordTemplate.SPEADSHEET_ID)
			        .execute();
			System.out.println("Spreadsheet ID: " + spreadsheet.getSpreadsheetId());
	        writeHeaders(service, spreadsheet);
		} catch(GeneralSecurityException e) {
			e.printStackTrace();
			return false;
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
        return true;
	}
	
	@PostMapping("/update")
	@CrossOrigin(origins = "http://localhost:4200")
	public boolean updateRecord(@RequestBody List<RecordUpdateRequest> records) {
		boolean recordsUpdated = true;
		try {
			WagerTypes.stream()
				.forEach(
					wagerType -> {
						try {
							writeToSheet(wagerType, records.stream().filter(record -> record.getMarket().equals(wagerType.getWagerType())).collect(Collectors.toList()), RECORD_ID);
						} catch (GeneralSecurityException | IOException e) {
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					}
			);
		} catch(RuntimeException e) {
			recordsUpdated = false;
		}
		return recordsUpdated;
	}
	
	@PostMapping("/update/models/{model}")
	@CrossOrigin(origins = "http://localhost:4200")
	public boolean updateModelRecords(@PathVariable("model") String model, @RequestBody List<EventRecordRequest> records) {
		boolean recordsUpdated = true;
		try {
			WagerTypes.stream()
				.forEach(
					wagerType -> {
						try {
							writeToSheet(wagerType, records.stream().filter(record -> record.getMarket().equals(wagerType.getWagerType())).collect(Collectors.toList()), PACE_MODEL_RECORDS_ID);
						} catch (GeneralSecurityException | IOException e) {
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					}
			);
		} catch(RuntimeException e) {
			recordsUpdated = false;
		}
		return recordsUpdated;
	}
	
	@PostMapping("/update/props")
	@CrossOrigin(origins = "http://localhost:4200")
	private boolean updatePropsRecords(@RequestBody List<PropRecordUpdateRequest> request) {
		boolean recordsUpdated = true;
		try {
			// Build a new GeneralSecurityException API client service.
	        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
	                .setApplicationName(APPLICATION_NAME)
	                .build();
			
			final List<List<Object>> values = new ArrayList<List<Object>>();
			request.forEach(prop -> values.add(prop.toPropRecord()));
			if(values.size() > 0) {
				ValueRange body = new ValueRange()
				        .setValues(values);
				
				AppendValuesResponse result =
		        service.spreadsheets().values().append(PROPS_RECORD_ID,"!A1", body)
		                .setValueInputOption(WagerRecordTemplate.USER_ENTERED)
		                .execute();
				System.out.printf("%d cells appended.", result.getUpdates().getUpdatedCells());
			}
		} catch(RuntimeException | GeneralSecurityException | IOException e) {
			e.printStackTrace();
			recordsUpdated = false;
		}
		return recordsUpdated;
	}
	
	private void writeToSheet(WagerTypes type, List<WagerRecord> wagersList, String sheetID) throws GeneralSecurityException, IOException {	
		if(wagersList.size() > 0) {
			// Build a new GeneralSecurityException API client service.
	        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
	                .setApplicationName(APPLICATION_NAME)
	                .build();
			
			final List<List<Object>> values = new ArrayList<List<Object>>();
			switch(type) {	
				case SPREAD:
					wagersList.forEach(wager -> values.add(wager.toSpreadRecord()));
					break;
				case MONEYLINE:
					wagersList.forEach(wager -> values.add(wager.toMoneylineRecord()));
					break;
				case TOTAL:
					wagersList.forEach(wager -> values.add(wager.toTotalRecord()));
					break;
				default:
					break;
			}
			
			
			if(values.size() > 0) {
				ValueRange body = new ValueRange()
				        .setValues(values);
				
				AppendValuesResponse result =
		        service.spreadsheets().values().append(sheetID, type.getWagerType() + "!A1", body)
		                .setValueInputOption(WagerRecordTemplate.USER_ENTERED)
		                .execute();
				System.out.printf("%d cells appended.", result.getUpdates().getUpdatedCells());
			}
		}
	}
	
	private void writeHeaders(Sheets service, Spreadsheet spreadsheet) throws IOException {		
		List<ValueRange> data = new ArrayList<>();
		Stream.of(WagerTypes.values()).forEach(
				wagerType -> {
					List<List<Object>> headers = null;
					switch(wagerType) {
						case SPREAD:
							headers = WagerRecordTemplate.spreadHeaders;
							break;
						case TOTAL:
							headers = WagerRecordTemplate.totalHeaders;
							break;
						case MONEYLINE:
							headers = WagerRecordTemplate.moneylineHeaders;
							break;
						default:
							break;
							
					}
					if(headers != null) {
						data.add(new ValueRange()
								.setRange(wagerType.getWagerType() + "!A1")
								.setValues(headers));	
					}
				}
			);
		
		Stream.of(WagerTypes.values()).forEach(
			wagerType -> {
				data.add(new ValueRange().setRange(wagerType.getWagerType() + "!L2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.resultVerticalHeaders));
				data.add(new ValueRange().setRange(wagerType.getWagerType() + "!O1").setValues(WagerRecordTemplate.resultHorizontalHeaders));
				if(wagerType.equals(WagerTypes.TOTAL)) {
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!N2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.totalsWinCalculations));
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!O2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.totalsWinsByEdge));	
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!P2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.totalsLosesByEdge));
				} else {
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!N2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.winCalculations));
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!O2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.winsByEdge));	
					data.add(new ValueRange().setRange(wagerType.getWagerType() + "!P2").setMajorDimension(WagerRecordTemplate.COLUMNS).setValues(WagerRecordTemplate.losesByEdge));	
				}
			});
		
		BatchUpdateValuesRequest body = new BatchUpdateValuesRequest()
		        .setValueInputOption(WagerRecordTemplate.USER_ENTERED)
		        .setData(data);
		BatchUpdateValuesResponse result =
		        service.spreadsheets().values().batchUpdate(spreadsheet.getSpreadsheetId(), body).execute();
		System.out.printf("%d cells updated.", result.getTotalUpdatedCells());
	}
}