package com.analytics.records.templates;

import java.util.Arrays;
import java.util.List;

public final class WagerRecordTemplate extends GoogleSheetsBaseTemplate {
	public static final List<List<Object>> spreadHeaders = Arrays.asList(Arrays.asList("Date", "Favorite", "Underdog", "Spread", "Odds", "Edge", "Result", "Payout"));
    public static final List<List<Object>> totalHeaders = Arrays.asList(Arrays.asList("Date", "Favorite", "Underdog", "Total", "Odds", "Pick", "Edge", "Result", "Payout"));
    public static final List<List<Object>> moneylineHeaders = Arrays.asList(Arrays.asList("Date", "Favorite", "Underdog", "Pick", "Odds", "Edge", "Result", "Payout"));
    
    public static final List<List<Object>> resultVerticalHeaders = Arrays.asList(Arrays.asList(
			"Win %:", "Win % on Edge > 5%:","Win % for Edge > 10%:", "Win % for Edge > 15%:", "Win % for Edge > 20%", "",
	"Net Total:","Net Total on %5","Net Total on 10%:","Net Total on 15%:","Net Total on 20%:"));
    public static final List<List<Object>> resultHorizontalHeaders = Arrays.asList(Arrays.asList("Wins", "Loses"));
    
    public static final List<List<Object>> winCalculations = Arrays.asList(
    		Arrays.asList("=COUNTIF(G2:G, \"W\") / COUNTIF(G2:G, \"*\")",
    				"=COUNTIFS(F2:F, \">=5.00%\", G2:G, \"W\") / COUNTIF(F2:F, \">=5.00%\")",
    				"=COUNTIFS(F2:F, \">=10.00%\", G2:G, \"W\") / COUNTIF(F2:F, \">=10.00%\")",
    				"=COUNTIFS(F2:F, \">=15.00%\", G2:G, \"W\") / COUNTIF(F2:F, \">=15.00%\")",
    				"=COUNTIFS(F2:F, \">=20.00%\", G2:G, \"W\") / COUNTIF(F2:F, \">=20.00%\")",
    				"",
    				"=SUM(H2:H)",
    				"=SUMIFS(H2:H, F2:F, \">=5.00%\")",
    				"=SUMIFS(H2:H, F2:F, \">=10.00%\")",
    				"=SUMIFS(H2:H, F2:F, \">=15.00%\")",
    				"=SUMIFS(H2:H, F2:F, \">=20.00%\")")
    );
    
    public static final List<List<Object>> totalsWinCalculations = Arrays.asList(
    		Arrays.asList("=COUNTIF(H2:H, \"W\") / COUNTIF(H2:H, \"*\")",
    				"=COUNTIFS(G2:G, \">=5.00%\", H2:H, \"W\") / COUNTIF(G2:G, \">=5.00%\")",
    				"=COUNTIFS(G2:G, \">=10.00%\", H2:H, \"W\") / COUNTIF(G2:G, \">=10.00%\")",
    				"=COUNTIFS(G2:G, \">=15.00%\", H2:H, \"W\") / COUNTIF(G2:G, \">=15.00%\")",
    				"=COUNTIFS(G2:G, \">=20.00%\", H2:H, \"W\") / COUNTIF(G2:G, \">=20.00%\")",
    				"",
    				"=SUM(I2:I)",
    				"=SUMIFS(I2:I, G2:G, \">=5.00%\")",
    				"=SUMIFS(I2:I, G2:G, \">=10.00%\")",
    				"=SUMIFS(I2:I, G2:G, \">=15.00%\")",
    				"=SUMIFS(I2:I, G2:G, \">=20.00%\")")
    );
    
    public static final List<List<Object>> winsByEdge = Arrays.asList(
    		Arrays.asList("=COUNTIF(G2:G, \"W\")",
    				"=COUNTIFS(F2:F, \">=5.00%\", G2:G, \"W\")",
    				"=COUNTIFS(F2:F, \">=10.00%\", G2:G, \"W\")",
    				"=COUNTIFS(F2:F, \">=15.00%\", G2:G, \"W\")",
    				"=COUNTIFS(F2:F, \">=20.00%\", G2:G, \"W\")")
    );
    
    public static final List<List<Object>> losesByEdge = Arrays.asList(
    		Arrays.asList("=COUNTIF(G2:G, \"L\")",
    				"=COUNTIFS(F2:F, \">=5.00%\", G2:G, \"L\")",
    				"=COUNTIFS(F2:F, \">=10.00%\", G2:G, \"L\")",
    				"=COUNTIFS(F2:F, \">=15.00%\", G2:G, \"L\")",
    				"=COUNTIFS(F2:F, \">=20.00%\", G2:G, \"L\")")
    );
    
    public static final List<List<Object>> totalsWinsByEdge = Arrays.asList(
    		Arrays.asList("=COUNTIF(H2:H, \"W\")",
    				"=COUNTIFS(G2:G, \">=5.00%\", H2:H, \"W\")",
    				"=COUNTIFS(G2:G, \">=10.00%\", H2:H, \"W\")",
    				"=COUNTIFS(G2:G, \">=15.00%\", H2:H, \"W\")",
    				"=COUNTIFS(G2:G, \">=20.00%\", H2:H, \"W\")")
    );
    
    public static final List<List<Object>> totalsLosesByEdge = Arrays.asList(
    		Arrays.asList("=COUNTIF(H2:H, \"L\")",
    				"=COUNTIFS(G2:G, \">=5.00%\", H2:H, \"L\")",
    				"=COUNTIFS(G2:G, \">=10.00%\", H2:H, \"L\")",
    				"=COUNTIFS(G2:G, \">=15.00%\", H2:H, \"L\")",
    				"=COUNTIFS(G2:G, \">=20.00%\", H2:H, \"L\")")
    );
}
