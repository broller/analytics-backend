package com.analytics.records.templates;

public class GoogleSheetsBaseTemplate {
	public static final String SPEADSHEET_ID = "spreadsheetId";
	
	public final static String COLUMNS = "COLUMNS"; 
	
	public final static String USER_ENTERED = "USER_ENTERED";
	
	public final static String ACCESS_TYPE_OFFLINE = "offline";
}
