package com.analytics.records.models;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.analytics.models.Totals;

public class PropRecordUpdateRequest {
	private String playerName;
	private String value;
	private double odds;
	private double edge;
	private Totals side;
	private String propType;
	
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public double getOdds() {
		return odds;
	}
	public void setOdds(double odds) {
		this.odds = odds;
	}
	public Totals getSide() {
		return side;
	}
	public void setSide(Totals side) {
		this.side = side;
	}
	public String getPropType() {
		return propType;
	}
	public void setPropType(String propType) {
		this.propType = propType;
	}
	public double getEdge() {
		return edge;
	}
	public void setEdge(double edge) {
		this.edge = edge;
	}
	private String setDate() {
		Format f = new SimpleDateFormat("MM/dd/yy");
	    return f.format(new Date());
	}
	public String getEdgeAsPercent() {
		return String.format("%.2f%%", getEdge() * 100); 
	}
	public List<Object> toPropRecord() {
		return Arrays.asList(setDate(), getPlayerName(), getPropType(), getValue(), getSide().toString(), getOdds(), getEdgeAsPercent());
	}
}
