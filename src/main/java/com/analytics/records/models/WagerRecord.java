package com.analytics.records.models;

import java.util.List;

public interface WagerRecord {
	public List<Object> toSpreadRecord();
	public List<Object> toTotalRecord();
	public List<Object> toMoneylineRecord();
}
