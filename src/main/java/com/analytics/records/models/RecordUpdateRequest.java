package com.analytics.records.models;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RecordUpdateRequest implements WagerRecord {
	private String market;
	private String favorite;
	private String underdog;
	private int odds;
	private double edge;
	private double spread;
	private double total;
	private String pick;
	
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getFavorite() {
		return favorite;
	}
	public void setFavorite(String favorite) {
		this.favorite = favorite;
	}
	public String getUnderdog() {
		return underdog;
	}
	public void setUnderdog(String underdog) {
		this.underdog = underdog;
	}
	public int getOdds() {
		return odds;
	}
	public void setOdds(int odds) {
		this.odds = odds;
	}
	public String getEdgeAsPercent() {
		return String.format("%.2f%%", getEdge() * 100); 
	}
	public double getEdge() {
		return edge;
	}
	public void setEdge(double edge) {
		this.edge = edge;
	}
	public double getSpread() {
		return spread;
	}
	public void setSpread(double spread) {
		this.spread = spread;
	}
	
	public List<Object> toSpreadRecord() {
		return Arrays.asList(setDate(), getFavorite(), getUnderdog(), getSpread(), getOdds(), getEdgeAsPercent());
	}
	
	public List<Object> toTotalRecord() {
		return Arrays.asList(setDate(), getFavorite(), getUnderdog(), getTotal(), getOdds(), getPick(), getEdgeAsPercent());
	}
	
	public List<Object> toMoneylineRecord() {
		return Arrays.asList(setDate(), getFavorite(), getUnderdog(), getPick(), getOdds(), getEdgeAsPercent());
	}
	
	private String setDate() {
		Format f = new SimpleDateFormat("MM/dd/yy");
	    return f.format(new Date());
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getPick() {
		return pick;
	}
	public void setPick(String pick) {
		this.pick = pick;
	}
}
