package com.analytics.sports;

import java.io.InputStreamReader;
import java.io.Reader;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;
import org.springframework.stereotype.Service;

@Service
public class TeamService {
	@Autowired
	private ResourceLoader resourceLoader;
	
	public String getTeamID(Leagues l, String name, String keyFileName, String site) {
		try {     
			  Resource resource = resourceLoader.getResource("classpath:" + keyFileName);
			  Reader reader = new InputStreamReader(resource.getInputStream());
			  String fileData =  FileCopyUtils.copyToString(reader);
			  JSONObject obj = new JSONObject(fileData).getJSONObject(site);
			  return obj.getString(name.replaceAll(" ", "_"));
	    } catch (Exception e) {     
	        e.printStackTrace();
	    }
		return null;
	}
}
