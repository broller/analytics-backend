package com.analytics.sports;

import java.io.InputStreamReader;
import java.io.Reader;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analytics.sports.models.League;
import com.analytics.sports.models.Team;

@RestController
@RequestMapping("/sports")
public class SportsController {
	@Autowired
	private ResourceLoader resourceLoader;
	
	@GetMapping("/{league}/teams")
	@CrossOrigin(origins = "http://localhost:4200")
	public Team[] getTeams(@PathVariable("league") Leagues league) {
      if(league == Leagues.MLB) {
      	return this.getLeague(Leagues.MLB, "MLBTeamKeys.json", "baseballReference").toArray();
      } else if(league == Leagues.NBA) {
      	return this.getLeague(Leagues.NBA, "NBATeamKeys.json", "basketballReference").toArray();
      }
      return null;
	}
	
	private League getLeague(Leagues l, String keyFileName, String site) {
		  try {     
			  Resource resource = resourceLoader.getResource("classpath:" + keyFileName);
			  Reader reader = new InputStreamReader(resource.getInputStream());
			  String fileData =  FileCopyUtils.copyToString(reader);
			  JSONObject obj = new JSONObject(fileData);
			  return printTeams(l, obj.getJSONObject(site));
		    } catch (Exception e) {     
		        e.printStackTrace();
		    }
		
		return null;
	}
	
	private League printTeams(Leagues leagueEnum, JSONObject teamKeys) {
		League l = new League();
		switch(leagueEnum) {
			case MLB:
				for (MLBTeams team : MLBTeams.values()) {
				    Team t = new Team(team.toString().replaceAll("_", " "), teamKeys.getString(team.toString()));
				    l.addTeam(team.toString(), t);
				}
				break;
			case NBA:
				for (NBATeams team : NBATeams.values()) {
				    Team t = new Team(team.toString().replaceAll("_", " "), teamKeys.getString(team.toString()));
				    l.addTeam(team.toString(), t);
				}
				break;
		}	
		return l;
	}
}
