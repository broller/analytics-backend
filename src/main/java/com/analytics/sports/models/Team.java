package com.analytics.sports.models;

public class Team {
	private String displayName;
	private String key;
	
	public Team(String displayName, String key) {
		this.displayName = displayName;
		this.key = key;
	}
	
	public String getTeamName() {
		return this.displayName;
	}
	
	public String getKey() {
		return this.key;
	}
}
