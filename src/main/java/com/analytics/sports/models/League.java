package com.analytics.sports.models;

import java.util.HashMap;

public class League {
	private HashMap<String, Team> teams;
	
	public League() {
		this.teams = new HashMap<String, Team>();
	}
	
	public League(HashMap<String, Team> teams) {
		this.teams = teams;
	}
	
	public HashMap<String, Team> getTeams() {
		return this.teams;
	}
	
	public Team getTeam(String key) {
		return this.teams.get(key);
	}
	
	public void addTeam(String key, Team t) {
		this.teams.put(key, t);
	}
	
	public Team[] toArray() {
		return this.teams.values().toArray(new Team[0]);
	}
}
