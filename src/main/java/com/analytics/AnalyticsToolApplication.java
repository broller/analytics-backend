package com.analytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalyticsToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalyticsToolApplication.class, args);
	}

}
