package com.analytics.models;

import java.util.HashMap;

import com.analytics.stats.models.SimulationResults;

public class Event {
	private String eventId;
	private HashMap<String, Market> markets;
	private SimulationResults simulationResults;
		
	public Event() {
		this.markets = new HashMap<String, Market>();
	}
	
	public void addMarket(String name, Market market) {
		this.markets.put(name, market);
	}
	
	public HashMap<String, Market> getMarkets() {
		return markets;
	}
	
	public Market getMarket(String name) {
		return this.markets.get(name);
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public SimulationResults getSimulationResults() {
		return simulationResults;
	}

	public void setSimulationResults(SimulationResults simulationsResults) {
		this.simulationResults = simulationsResults;
	}
}
