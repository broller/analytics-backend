package com.analytics.models;

import com.analytics.holds.HoldsCalculationService;

public class Hold extends Market {
	private double hold;
	
	public Hold(Side ...sides) {
		super(sides);
		this.hold = HoldsCalculationService.calculateHold(this.getSides().get(0).getOdds(), this.getSides().get(1).getOdds());
	}

	public double getHold() {
		return hold;
	}
}
