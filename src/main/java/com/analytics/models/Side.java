package com.analytics.models;

public class Side {
	private String teamName;
	private double points, odds;
	private String overUnder;
	
	public Side(String teamName, double points, double odds) {
		this.setTeamName(teamName);
		this.points = points;
		this.odds = odds;
	}
	
	public Side(String teamName, double points, double odds, String overUnder) {
		this.setTeamName(teamName);
		this.points = points;
		this.odds = odds;
		this.overUnder = overUnder;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public double getPoints() {
		return points;
	}

	public double getOdds() {
		return odds;
	}

	public String getOverUnder() {
		return overUnder;
	}
}
