package com.analytics.models;

import java.util.LinkedList;

public class Market {
	private LinkedList<Side> sides;
	
	public Market(Side ...sides) {
		this.sides = new LinkedList<Side>();
		for(Side side: sides) {
			this.sides.add(side);
		}
	}

	public LinkedList<Side> getSides() {
		return this.sides;
	}
}
