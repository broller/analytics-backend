# Analytics Backend Overview
A middleware piece connecting the front-end with the python based [monte carlo simulations](https://bitbucket.org/broller/nba-monte-carlo-simulation/src/main/). The project takes responses from the front-end and converts them intom command line arguments executed within the shell and returned to the front-end.

## Setup
Run `mvn install && mvn package`. Navigate to the target directory and run `java -jar analytics-tool-0.0.1-SNAPSHOT.jar`.